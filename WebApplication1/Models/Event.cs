﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace WebApplication1.Models
{
    public class Event
    {
        [ScaffoldColumn(false)]
        [Key]
        public int EventID { get; set; }

        [Display(Name = "Nazwa wydarzenia")]
        [Required(ErrorMessage = "Musisz wprowadzić nazwę")]
        [StringLength(30)]
        public string Name { get; set; }

        [Display(Name = "Miasto")]
        [Required(ErrorMessage = "Musisz podać miasto")]
        [StringLength(30)]
        public string City { get; set; }

        [Display(Name = "Ilość biletów")]
        [Required(ErrorMessage = "Musisz podać ilość")]
        public int NumberOfTickets { get; set; }

        [Display(Name = "Data wydarzenia")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Required(ErrorMessage = "Musisz podać date")]
        public DateTime? TimeDate { get; set; }

        [Display(Name = "Odpłatność")]
        [Required(ErrorMessage = "Wprowadź informację")]
        public string Payment { get; set; }

        [Display(Name = "Cena biletu")]
        [Required(ErrorMessage = "Podaj cene")]
        public int? Price { get; set; }

        [EmailAddress]
        [Required(ErrorMessage = "Musisz wprowadzić email kontakowy")]
        public string Email { get; set; }

        [Phone]
        [RegularExpression(@"([\+]){0,1}([0-9]{2})?[\-\s]?[-]?([0-9]{3})\-?[-\s]?([0-9]{3})[-\s]\-?([0-9]{3})$",
                            ErrorMessage = "Numer musi być zapisany w formacie 123-123-123")]
        [Display(Name = "Numer kontaktowy")]
        public string Number { get; set; }

        public string Organizer { get; set; }
        public virtual ICollection<ApplicationUser> AplicationUsers { get; set; }

        public override bool Equals(object obj)
        {
            Event first = obj as Event;
            if (first == null)
                return false;
            else
                return Name.Equals(first.Name) && City.Equals(first.City) && TimeDate.Equals(first.TimeDate) && Payment.Equals(first.Payment);
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode() ^ this.City.GetHashCode() ^ this.TimeDate.GetHashCode() ^ this.Payment.GetHashCode();
        }
    }

    public class Search
    {
        [Display(Name = "Data wydarzenia")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Required(ErrorMessage = "Musisz podać date")]
        public DateTime? TimeDate { get; set; }

        [Display(Name = "Miasto")]
        [Required(ErrorMessage = "Musisz podać miasto")]
        [StringLength(30)]
        public string City { get; set; }
    }
}