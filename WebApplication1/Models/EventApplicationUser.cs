﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class EventApplicationUser
    {
        [ScaffoldColumn(false)]
        [Key]
        public int ID { get; set; }
        public string IDUser { get; set; }
        public int IDEvent { get; set; }

        public override bool Equals(object obj)
        {
            EventApplicationUser first = obj as EventApplicationUser;
            if (first == null)
                return false;
            else
                return IDUser.Equals(first.IDUser) && IDEvent.Equals(first.IDEvent);
        }
        public override int GetHashCode()
        {
            return this.IDUser.GetHashCode() ^ this.IDEvent.GetHashCode();
        }
    }
}