﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using System.Net.Mail;

namespace WebApplication1.Controllers
{

    public class EventController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Event
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            return View(db.Events.ToList());
        }
        [Authorize]
        public ActionResult CreateEvent()
        {
            Event FirstEvent = new Event();
            return View(FirstEvent);
        }
        [Authorize]
        [HttpPost]
        public ActionResult CreateEvent(Event FirstEvent)
        {
            if (!ModelState.IsValid)
            {
                return View(FirstEvent);
            }
            else
            {
                foreach (var item in db.Events)
                {
                    if (FirstEvent.Equals(item))
                    {
                        ViewBag.Message = "Istnieje takie wydarzenie!";
                        return View();
                    }
                }
                FirstEvent.Organizer = User.Identity.GetUserId();
                ViewBag.Message = "Stworzono wydarzenie " + FirstEvent.Name;
                db.Events.Add(FirstEvent);
                db.SaveChanges();
                return View();
            }
        }
        public ActionResult Edit(int? id)
        {            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event EditEvent = db.Events.Find(id);
            if (EditEvent.Organizer != User.Identity.GetUserId())
            {
                return View("Error");
            }
            if (EditEvent == null)
            {
                return HttpNotFound();
            }
            return View(EditEvent);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EventID,Organizer,Name,City,NumberOfTickets,TimeDate,Payment,Price,Email,Number")] Event EditEvent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(EditEvent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ImOrganizer");
            }
            return View(EditEvent);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event DeleteEvent = db.Events.Find(id);
            if (DeleteEvent.Organizer != User.Identity.GetUserId())
            {
                return View("Error");
            }
            if (DeleteEvent == null)
            {
                return HttpNotFound();
            }
            return View(DeleteEvent);
        }

        // POST: strudents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Event DeleteEvent = db.Events.Find(id);
            db.Events.Remove(DeleteEvent);
            db.SaveChanges();
            return RedirectToAction("ImOrganizer");
        }
        [Authorize]
        public ActionResult ImOrganizer()
        {
            string userID = User.Identity.GetUserId();
            var MyEvents = new List<Event>();
            foreach (var item in db.Events)
            {
                if(item.Organizer == userID)
                   MyEvents.Add(db.Events.Find(item.EventID));
            }
            return View(MyEvents);
        }
        [Authorize]
        public ActionResult SignUp(int id)
        {
            string userID = User.Identity.GetUserId();
            EventApplicationUser first = new EventApplicationUser
            {
                IDEvent = id,
                IDUser = userID
            };
            foreach(var item in db.EventApplicationUsers)
            {
                if (first.Equals(item))
                {
                    ViewBag.Message = "Jestes zapisany na to wydarzenie!";
                    return View("Success");
                }
            }
            Event ReducingTickets = db.Events.Find(id);

                if (ReducingTickets.NumberOfTickets > 0)
                {
                    db.EventApplicationUsers.Add(first);
                    ReducingTickets.NumberOfTickets -= 1;
                    db.SaveChanges();
                }
                return View("Success");
        }
        [Authorize]
        public ActionResult SignOff(int id)
        {
            string userID = User.Identity.GetUserId();
            int key = 0;
            Event IncreaseTickets = db.Events.Find(id);
            foreach(var item in db.EventApplicationUsers)
            {
                if (item.IDEvent == id && item.IDUser == userID)
                {
                    key = item.ID;
                    break;
                }
            }
            EventApplicationUser removeUser = db.EventApplicationUsers.Find(key);
            db.EventApplicationUsers.Remove(removeUser);
            IncreaseTickets.NumberOfTickets += 1;
            db.SaveChanges();
            return View("Success");
        }
        [Authorize]
        public ActionResult MyEvents()
        {
            int[] tab = new int[db.EventApplicationUsers.Count()];
            int i = 0;
            string userID = User.Identity.GetUserId();
            var myEvents = new List<Event>();
            foreach(var item in db.EventApplicationUsers)
            {
                if (item.IDUser == userID)
                {
                    tab[i] = item.IDEvent;
                    i++;
                }
            }
            for(int j=0; j<i; j++)
            {
                myEvents.Add(db.Events.Find(tab[j]));
            }
            return View(myEvents);
        }
        public ActionResult Search()
        {
            Search SearchEvents = new Search();
            return View(SearchEvents);
        }
        [HttpPost]
        public ActionResult Search(Search SearchEvents)
        {
            var events = new List<Event>();
            if (!ModelState.IsValid)
            {
                return View(SearchEvents);
            }
            else
            {
                foreach(var item in db.Events)
                {
                    if(item.City == SearchEvents.City && item.TimeDate == SearchEvents.TimeDate)
                    {
                        events.Add(item);
                    }
                }
                if (events.Count() == 0)
                {
                    ViewBag.Message = "Nie znaleziono żadnego wydarzenia ";
                    return View();
                }
                else
                    return View("List", events);
            }
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event FirstEvent = db.Events.Find(id);
            if (FirstEvent == null)
            {
                return HttpNotFound();
            }
            return View(FirstEvent);

        }
        public FileResult CreatePdf(int? id)
        {
            MemoryStream workStream = new MemoryStream();
            StringBuilder status = new StringBuilder("");
            DateTime dTime = DateTime.Now;
            //file name to be created   
            Event FirstEvent = db.Events.Find(id);
            string strPDFFileName = string.Format(FirstEvent.Name + dTime.ToString("yyyyMMdd") + "_mgjmas" + ".pdf");
            Document doc = new Document(PageSize.A5, 50, 50, 25, 25);
            doc.SetMargins(10f, 10f, 10f, 10f);
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, false);
            Font times = new Font(bfTimes, 12);
            Font times2 = new Font(bfTimes, 16);
            string Email = "Email: " + FirstEvent.Email;
            string EventName = "Nazwa wydarzenia: " + FirstEvent.Name;
            string City = "Miasto: " + FirstEvent.City;
            string EventDate = "Data wydarzenia: " + FirstEvent.TimeDate.Value.ToString("dd/MM/yyyy");
            string FreePaid = "Odpłatność: " + FirstEvent.Payment;
            string Price = "Cena biletu: " + FirstEvent.Price + "zł";
            Paragraph paraEmail = new Paragraph(Email, times2);
            Paragraph paraEventName = new Paragraph(EventName, times2);
            Paragraph paraCity = new Paragraph(City, times2);
            Paragraph paraEventDate = new Paragraph(EventDate, times2);
            Paragraph paraFreePaid = new Paragraph(FreePaid, times2);
            Paragraph paraPrice = new Paragraph(Price, times2);
            //Create PDF Table with 5 columns  
            PdfPTable tableLayout = new PdfPTable(6);
            doc.SetMargins(20f, 20f, 20f, 20f);
            //Create PDF Table  

            //file will created in this path  
            string strAttachment = Server.MapPath("~/Downloadss/" + strPDFFileName);


            PdfWriter.GetInstance(doc, workStream).CloseStream = false;
            doc.Open();
            Paragraph title = new Paragraph("BILET", new Font(Font.FontFamily.HELVETICA, 20, 1, new iTextSharp.text.BaseColor(0, 0, 0)));
            title.Alignment = Element.ALIGN_MIDDLE;
            doc.Add(title);
            doc.Add(paraEmail);
            doc.Add(paraEventName);
            doc.Add(paraCity);
            doc.Add(paraEventDate);
            doc.Add(paraFreePaid);
            doc.Add(paraPrice);
            string QrUrl = "http://pawtnet.azurewebsites.net/Event/Details/" + FirstEvent.EventID;
            BarcodeQRCode barcodeQRCode = new BarcodeQRCode(QrUrl, 1000, 1000, null);
            Image codeQrImage = barcodeQRCode.GetImage();
            codeQrImage.ScaleAbsolute(165, 165);
            //doc.Add(Add_Content_To_PDF(tableLayout, id));
            doc.Add(codeQrImage);
            // Closing the document  
            doc.Close();

            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;


            return File(workStream, "application/pdf", strPDFFileName);

        }

        protected PdfPTable Add_Content_To_PDF(PdfPTable tableLayout, int? id)
        {

            float[] headers = { 50, 24, 45, 35, 50, 40 }; //Header Widths  
            tableLayout.SetWidths(headers); //Set the pdf headers  
            tableLayout.WidthPercentage = 90; //Set the PDF File witdh percentage  
            tableLayout.HeaderRows = 1;
            //Add Title to the PDF file at the top  
            Event FirstEvent = db.Events.Find(id);
            ApplicationUser user = db.Users.Find(User.Identity.GetUserId());

            tableLayout.AddCell(new PdfPCell(new Phrase("BILET", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(0, 0, 0))))
            {
                Colspan = 12,
                Border = 0,
                PaddingBottom = 5,
                HorizontalAlignment = Element.ALIGN_CENTER
            });


            //////Add header  
            //AddCellToHeader(tableLayout, "Email");
            //AddCellToHeader(tableLayout, "Nazwa wydarzenia");
            //AddCellToHeader(tableLayout, "Miasto");
            ////AddCellToHeader(tableLayout, "Ilość biletów");
            //AddCellToHeader(tableLayout, "Data wydarzenia");
            //AddCellToHeader(tableLayout, "Odpłatność");
            //AddCellToHeader(tableLayout, "Cena biletu");
            ////AddCellToHeader(tableLayout, "Email");
            ////AddCellToHeader(tableLayout, "Numer kontaktowy");
            //////Add body  
            ////Paragraph Text = new Paragraph(" Zakaz palenia");
            //AddCellToBody(tableLayout, user.UserName);
            //AddCellToBody(tableLayout, FirstEvent.Name);
            //AddCellToBody(tableLayout, FirstEvent.City);
            ////AddCellToBody(tableLayout, FirstEvent.NumberOfTickets.ToString());
            //AddCellToBody(tableLayout, FirstEvent.TimeDate.ToString());
            //AddCellToBody(tableLayout, FirstEvent.Payment);
            //AddCellToBody(tableLayout, FirstEvent.Price.ToString());
            //// AddCellToBody(tableLayout, FirstEvent.Email.ToString());
            //// if(FirstEvent.Number != null)
            ////AddCellToBody(tableLayout, FirstEvent.Number.ToString());



            return tableLayout;
        }
        // Method to add single cell to the Header  
        private static void AddCellToHeader(PdfPTable tableLayout, string cellText)
        {
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, false);
            Font times = new Font(bfTimes, 12);
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, times))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Padding = 5,
                BackgroundColor = new iTextSharp.text.BaseColor(128, 0, 0)
            });
        }

        // Method to add single cell to the body  
        private static void AddCellToBody(PdfPTable tableLayout, string cellText)
        {
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, false);
            Font times = new Font(bfTimes, 12);
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, times))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Padding = 5,
                BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255)
            });
        }

        [Authorize]
        public ActionResult Sendemails(int id)
        {
            Event SendEmail = db.Events.Find(id);
            if (SendEmail.Organizer != User.Identity.GetUserId())
            {
                return View("Error");
            }
            TempData["id"] = id;

            return View();
        }
        [Authorize]
        [HttpPost]
        public ActionResult Sendemails(MailViewModel mvm)
        {
            mvm.EventID = Convert.ToInt32(TempData["id"]);

            MailMessage mm = new MailMessage();
            mm.From = new MailAddress("projekt.pb.2019@gmail.com");
            mm.Subject = mvm.Subject;
            mm.Body = mvm.Body;
            mm.IsBodyHtml = false;

            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

            NetworkCredential nc = new NetworkCredential("projekt.pb.2019@gmail.com", "ProjektPB2019");
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = nc;

            string[] tab = new string[db.EventApplicationUsers.Count()];
            int i = 0;
            var usersonevent = new List<ApplicationUser>();
            foreach (var item in db.EventApplicationUsers)
            {
                if (item.IDEvent == mvm.EventID)
                {
                    tab[i] = item.IDUser;
                    i++;
                }
            }

            for (int j = 0; j < i; j++)
            {
                usersonevent.Add(db.Users.Find(tab[j]));
            }

            foreach (var item in usersonevent)
            {
                mm.To.Add(new MailAddress(item.Email));
                smtp.Send(mm);
            }

            ViewBag.Message = "E-maile zostały rozesłane poprawnie!";

            return View();
        }
    }
}