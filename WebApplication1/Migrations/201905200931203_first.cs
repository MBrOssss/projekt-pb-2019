namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "City", c => c.String(nullable: false, maxLength: 30));
            AddColumn("dbo.Events", "NumberOfTickets", c => c.Int(nullable: false));
            AddColumn("dbo.Events", "TimeDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "Payment", c => c.String(nullable: false));
            AddColumn("dbo.Events", "Price", c => c.Int(nullable: false));
            AddColumn("dbo.Events", "Email", c => c.String(nullable: false));
            AddColumn("dbo.Events", "Number", c => c.String());
            AddColumn("dbo.Events", "Organizer", c => c.String());
            AlterColumn("dbo.Events", "Name", c => c.String(nullable: false, maxLength: 30));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Events", "Name", c => c.String());
            DropColumn("dbo.Events", "Organizer");
            DropColumn("dbo.Events", "Number");
            DropColumn("dbo.Events", "Email");
            DropColumn("dbo.Events", "Price");
            DropColumn("dbo.Events", "Payment");
            DropColumn("dbo.Events", "TimeDate");
            DropColumn("dbo.Events", "NumberOfTickets");
            DropColumn("dbo.Events", "City");
        }
    }
}
