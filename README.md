[**https://pawtnet3.azurewebsites.net**](https://pawtnet3.azurewebsites.net)

**Instrukcja uruchomienia:**
1.  Pobierz repozytorium
2.  Rozpakuj
3.  Wejdź w nowo utworzony folder
4.  Otwórz projekt - WebApplication1.sln
5.  Uruchom aplikacje z debugowaniem (domyślnie F5) lub bez debugowania (domyślnie Ctrl+F5)


**Wykorzystane biblioteki:**
* bootstrap v4.3.1 - biblioteka CSS, zawiera czcionki i JavaScript
* EntityFramework v6.2.0 - rekomendowany przez Microsoft do dostępu do danych dla nowych aplikacji
* iTextSharp v5.5.13 - opensource'owa biblioteka do generowanie PDF'ów na platformie .NET
* jQuery v3.4.1 - biblioteka JavaScript
* jQuery.UI.Combined v1.12.1 - pełna biblioteka jQuery w jednym pliku
* jQuery.Validation v1.17.0 - plugin wspomagający walidację formularzy
* Microsoft.AspNet.Identity.Core v.2.2.2 - najpotrzebniejsze interfejsy dla ASP.NET Identity
* Microsoft.AspNet.Identity.EntityFramework v2.2.2
* Microsoft.AspNet.Identity.Owin v2.2.2.2
* Microsoft.AspNet.Mvc 5.2.7
* Microsoft.AspNet.Razor v3.2.7
* Microsoft.AspNet.TelemetryCorrelation v1.0.0
* Microsoft.AspNet.Web.Optimization v1.1.3
* Microsoft.AspNet.WebApi v5.2.4
* Microsoft.AspNet.WebApi.Client v5.2.7
* Microsoft.AspNet.WebApi.Core v5.2.7
* Microsoft.AspNet.WebApi.HelpPage v5.2.7
* Microsoft.AspNet.WebApi.WebHost v5.2.7
* Microsoft.AspNet.WebPages v3.2.7
* Microsoft.CodeDom.Providers.DotNetCompilerPlatform v2.0.0
* Microsoft.jQuery.Unobtrusive.Validation v3.2.4
* Microsoft.Owin v4.0.0
* Microsoft.Owin.Host.SystemWeb v4.0.0
* Microsoft.Owin.Security v4.0.0
* Microsoft.Owin.Security.Cookies v4.0.0
* Microsoft.Owin.Security.Facebook v4.0.0
* Microsoft.Owin.Security.Google v4.0.0
* Microsoft.Owin.Security.MicrosoftAccount v4.0.0
* Microsoft.Owin.Security.OAuth v4.0.0
* Microsoft.Owin.Security.Twitter v4.0.0
* Microsoft.Web.Infrastructure v4.0.0
* Modernizr v2.8.3 - wspomaganie starych przeglądarek
* Newtonsoft.Json v11.0.1 - JSON framework dla .NET
* Owin v1.0.0
* System.Diagnostics.DiagnosticSource v4.4.1
* WebGrease v1.6.0
* .Net v4.7
* Windows 10
* 